import { HunterModel } from "./actorModels/hunter.js"


export class ActorModel extends Actor {
  constructor(...args) {
    super(...args);

    if (this.data.type === "hunter") {
      Object.assign(this, HunterModel);
    }
  }

  // When creating a new Hunter, add basic moves to it
  // We skip it if the Hunter has already some moves in DB
  // We add default token configuration as well
  static async create(data, options) {
    if (data.type !== "hunter") return;

    if (!data.items) {
      const lang = game.i18n.lang;
      const pack = game.packs.find(p => p.collection === `motw.basic-moves-${lang}` || p.collection === 'motw.basic-moves-fr');

      data.items = await pack.getContent()
                             .then(contents => contents.map(i => i.data))
    }

    data.token = data.token || {};
    mergeObject(data.token, {
      vision: true,
      dimSight: 30,
      brightSight: 0,
      actorLink: true,
      disposition: 1
    }, {overwrite: false});

    super.create(data, options);
  }

  rollAttribute(attributeName) {
    const label = game.i18n.localize(`MOTW.${attributeName}`)
    const attributeValue = this.attributes[attributeName].value;
    const attributeBonus = this.attributes[attributeName].mod;
    const roll = new Roll(`2d6+${attributeValue}+${attributeBonus}`).roll();

    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this }),
      flavor: `${label}`
    });

    return roll;
  }

  get attributes() {
    return this.data.data.attributes;
  }

  get inventory() {
    return this.itemTypes.item;
  }
}

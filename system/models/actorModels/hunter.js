export const HunterModel = {


// Each time a career path is changed,
  // We update the hunter's attributes
  updateAttributes() {
    const newData = {data: {attributes: {}}};
    const startingAttributes = this.archetype().attributes();

    ["charm", "cool", "sharp", "tough", "weird"].forEach((attribute, index) => newData.data.attributes[attribute] = {value: startingAttributes[index]});

    this.improvements().forEach((improvement) => {
      if (!improvement.isChangingAttributes()) return;

      newData.data.attributes[improvement.improved()].value += improvement.isAdvanced() ? 3 : 1;
    });
    this.update(newData);
  },

  // Updating the resources
  // Taking the 4th hit put you in an unstable state
  updateResources(resource, delta) {
    const newData = {data: {}};
    const newResourceValue = this.data.data[resource].value + delta;

    newData.data[resource] = {value: newResourceValue};
    if (resource === "health" && newResourceValue === 4 && delta > 0) {
      newData.data.health.unstable = true;
    }

    this.update(newData);
  },

  toggleBonusAttribute(attributeName) {
    const newData = {data: {attributes: {}}};
    newData.data.attributes[attributeName] = {};
    console.log(attributeName)
    const attributeMod = this.attributes[attributeName].mod;

    newData.data.attributes[attributeName].mod = Math.abs(attributeMod - 1);
    this.update(newData);
  },

  archetype() {
    return this.itemTypes.archetype[0] || null;
  },

  basicMoves() {
    return this.itemTypes.move.filter(m => m.isBasicMove());
  },

  hunterMoves() {
    return this.itemTypes.move.filter((m) => {
      return !m.isBasicMove() && m.archetype() === this.archetype().codeName
    });
  },

  alienMoves() {
    return this.itemTypes.move.filter((m) => {
      return !m.isBasicMove() && m.archetype() !== this.archetype().codeName
    });
  },

  moves() {
    return this.itemTypes.move
  },

  improvements() {
    return this.itemTypes.improvement;
  },

  maxHunterMoves() {
    const moveAttributes = this.improvements().reduce((acc, improvement) => {
      return improvement.improved() === "hunterMoves" ? acc + 1 : acc
    }, 0)

    const archetypeMoves = this.archetype()?.moves() || 0
    return archetypeMoves + moveAttributes;
  },

  maxAlienMoves() {
    return this.improvements().reduce((acc, improvement) => {
      return improvement.improved() === "alienMoves" ? acc + 1 : acc
    }, 0)
  },

  weapons() {
    return this.itemTypes.weapon
  },

  armors() {
    return undefined;
  },
}

export const ImprovementModel = {


  improved() {
    const improvement = this.data;
    return improvement.data.improved;
  },

  isAdvanced() {
    const improvement = this.data;
    return improvement.data.advanced;
  },

  archetypes() {
    const improvement = this.data;
    return Object.keys(improvement.data.archetypes);
  },

  maxOwnedForArchetype(archetype) {
    const improvement = this.data;
    return improvement.data.archetypes[archetype];
  },

  isChangingAttributes() {
    return ["charm", "cool", "sharp", "tough", "weird"].includes(this.improved());
  },

  maxAttributeForArchetype(archetype) {
    const improvement = this.data;
    return improvement.data.max[archetype];
  },
}

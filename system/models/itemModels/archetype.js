export const ArchetypeModel = {


  attributes() {
    const archetype = this.data;
    return archetype.data.stats[archetype.data.selected];
  },

  moves() {
    const archetype = this.data;
    return archetype.data.movesAtStart;
  },
}

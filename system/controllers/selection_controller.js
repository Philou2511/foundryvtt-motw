export class SelectionController extends Application {
    constructor(actor, pack, options = {}) {
    super(options);
    this.actor = actor;
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      tabs: [],
      classes: ["motw", "frame"],
      width: 350,
      height: 650,
      left: 15,
      top: 300,
      resizable: true,
      minimizable: false,
    });

    return options;
  }

  onDragStart(event, dragData, li) {
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    event.dataTransfer.setDragImage(li, 25, 25);
  }

  activateListeners(html) {
    super.activateListeners(html);

    const frameId = this._element[0].id;
    const content = document.getElementById(frameId);

    // Make every ".drag" element draggable
    content.querySelectorAll('.drag').forEach((element) => {
      element.setAttribute("draggable", true);
      element.addEventListener("dragstart", (event) => {
        this.onDragStart(event);
      }, false);
    });
  }
}

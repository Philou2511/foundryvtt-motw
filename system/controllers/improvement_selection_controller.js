import { SelectionController } from "./selection_controller.js";

export class ImprovementSelectionController extends SelectionController {


  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      template: "systems/motw/system/views/improvement/selection.html",
      title: game.i18n.localize("MOTW.Improvements")
    });

    return options;
  }

  async getData() {
    const data = super.getData();
    const pack = game.packs.find(p => p.collection === "motw.improvements");
    const content = await pack.getContent();

    data.improvements = content.filter(e => this.filter(e));

    return data;
  }

  filter(improvement) {
    // No advanced improvements before the 5th upgrade
    if (this.actor.improvements().length < 4 && improvement.isAdvanced()) return false;

    // No improvements unavailable to the character's archetype
    const availableArchetypes = improvement.archetypes();
    const archetype = this.actor.archetype().codeName
    if (!availableArchetypes.includes(archetype)) return false;

    // No improvements that have already been picked up too much
    const sameImprovements = this.actor.improvements().filter(e => e.codeName === improvement.codeName);
    if (sameImprovements.length >= improvement.maxOwnedForArchetype(archetype)) return false;

    // No improvement that would get you above the maximum for an attribute
    const attribute = improvement.improved();
    const max = improvement.maxAttributeForArchetype(archetype);
    if (improvement.isChangingAttributes() && this.actor.attributes[attribute].value >= max) return false;

    return true;
  }

  onDragStart(event) {
    const li = event.currentTarget.closest(".directory-item");
    const dragData = { type: "Improvement", pack: "motw.improvements", id: li.dataset.entryId, actorTarget: this.actor._id };
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    super.onDragStart(event, dragData, li);
  }
}

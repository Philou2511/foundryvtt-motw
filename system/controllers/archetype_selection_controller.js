import { SelectionController } from "./selection_controller.js";

export class ArchetypeSelectionController extends SelectionController {


  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      template: "systems/motw/system/views/archetype/selection.html",
      title: game.i18n.localize("MOTW.Archetypes")
    });

    return options;
  }

  async getData() {
    const data = super.getData();
    const pack = game.packs.find(p => p.collection === "motw.archetypes");
    const content = await pack.getContent();

    if (this.actor.archetype()) return null;

    data.archetypes = content;
    return data;
  }

  onDragStart(event) {
    const li = event.currentTarget.closest(".directory-item");
    const dragData = { type: "Archetype", pack: "motw.archetypes", id: li.dataset.entryId, actorTarget: this.actor._id };
    super.onDragStart(event, dragData, li);
  }
}

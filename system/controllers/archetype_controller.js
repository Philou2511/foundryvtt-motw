export class ArchetypeController extends ItemSheet {


  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "archetype-sheet"],
      template: "systems/motw/system/views/archetype/sheet.html",
      width: 520,
      height: 480,
    });
  }
}

import { SelectionController } from "./selection_controller.js";

export class MoveSelectionController extends SelectionController {


  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      template: "systems/motw/system/views/move/selection.html",
      title: game.i18n.localize("MOTW.Moves")
    });

    return options;
  }

  async getData() {
    const data = super.getData();
    const packs = game.packs.filter(p => p.collection.includes("moves"));
    const content = await packs.reduce(async (result, pack) => {
      const moves = await pack.getContent();
      const promisedResult = await result;

      return promisedResult.concat(moves);
    }, []);

    const moves = this.sortByCategory(content);
    data.moves = this.selectAvailable(moves);

    return data;
  }

  sortByCategory(moves) {
    return moves.reduce((result, move) => {
      const key = move.archetype() || "BasicMoves";
      if (result[key]) {
        result[key].push(move);
      } else {
        result[key] = [move];
      }

      return result;
    }, {})
  }

  selectAvailable(moves) {
    Object.keys(moves).forEach((archetype) => {
      moves[archetype] = moves[archetype].filter(move => this.filter(move))
    })
    return moves;
  }

  filter(move) {
    // Cannot have the same move twice
    const moveNames = this.actor.moves().map(m => m.codeName);
    if (moveNames.includes(move.codeName)) return false;

    // Cannot learn more moves than maximum for own archetype
    const archetype = this.actor.archetype().codeName;
    const hunterMovesCount = this.actor.hunterMoves().length;
    const maxHunterMoves = this.actor.maxHunterMoves();
    if (move.archetype() === archetype && hunterMovesCount >= maxHunterMoves) return false;

    // Cannot learn more moves than maximum for other archetypes
    const alienMovesCount = this.actor.alienMoves().length;
    const maxAlienMoves = this.actor.maxAlienMoves();
    if (move.archetype() !== archetype && alienMovesCount >= maxAlienMoves) return false;

    return true;
  }

  onDragStart(event) {
    const li = event.currentTarget.closest(".directory-item");
    const dragData = { type: "Move", pack: li.dataset.pack, id: li.dataset.entryId, actorTarget: this.actor._id };
    super.onDragStart(event, dragData, li);
  }
}

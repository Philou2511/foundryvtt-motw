const registerHelpers = () => {
  Handlebars.registerHelper("add", (a, b) => a + b);
  Handlebars.registerHelper("subtract", (a, b) => a - b);
  Handlebars.registerHelper("multiply", (a, b) => a * b);

  Handlebars.registerHelper("eq", (a, b) => a === b);
  Handlebars.registerHelper("or", (a, b) => a || b);

  Handlebars.registerHelper('toLowerCase', str => str.toLowerCase());
  Handlebars.registerHelper('concat', (...args) => {
    return args.reduce((result, arg) => typeof arg == 'object' ? result : `${result}${arg}`, "")
  });

  Handlebars.registerHelper('parseHTML', (str) => {
    const parsedHTML = Handlebars.compile(str, { strict: true, knownHelpers: {'icon': true} });
    return TextEditor.enrichHTML(parsedHTML({}));
  });

  Handlebars.registerHelper('parseHTML', (str) => {
    const parsedHTML = Handlebars.compile(str, { strict: true, knownHelpers: {'icon': true} });
    return TextEditor.enrichHTML(parsedHTML({}));
  });

  Handlebars.registerHelper('color', (str, color) => {
    return `<span style="color: ${CONFIG.MOTW[color] || color};">${str}</span>`
  });

  Handlebars.registerHelper('icon', (str) => {
    if (str === "dice") return '<i class="fas fa-dice"></i>';
    if (str === "trash") return '<i class="fas fa-trash"></i>';
    if (str === "plus") return '<i class="fas fa-plus"></i>';
  });

}

export { registerHelpers };

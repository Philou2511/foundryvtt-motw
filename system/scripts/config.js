const MOTW = {}

MOTW.colors = {
  "red": "#EB606F",
  "orange": "#F49300",
  "green": "#8BC66F"
}

export { MOTW };
